var request = require('request');
var crypto = require('./crypto-credentials');

var getArtBuild = function (req, res) {
    var repoUuid = req.query.repoUuid;
    var builds;
    console.log('FOUND UUID' + repoUuid);
    artifactoryRequestOptions('build', repoUuid, function (options) {
        if (options != null) {

            function callback(error, response, body) {
                if (!error && response.statusCode === 200) {
                    var list = JSON.parse(body);
                    builds = list["builds"];
                    var ArtifactoryBuild = getArtifactoryBuild();
                    ArtifactoryBuild.findOne({where: {repoUuid: repoUuid}}).then(data => {
                        if (data == null) {
                            res.render('associate-artifactory-build', {repoUuid: repoUuid, builds: builds})
                        } else {
                            var selected = [{'uri': data.artifactoryBuild}];
                            res.render('associate-artifactory-build', {
                                repoUuid: repoUuid,
                                builds: builds,
                                selected: selected
                            })
                        }
                    }).catch(function (error) {
                        res.send('Error occurred while querying for an already existing associated Artifactory Build: ' + error);
                    });
                } else {
                    console.log('Got error fetching builds from Artifactory:' + error);
                }
            }

            request(options, callback);
        } else {
            var message = 'configured Artifactory user not found! ';
            res.render('error', {message: message});
        }
    });
};

var postArtBuild = function (req, res) {
    var repoUuid = req.body.repoUuid;
    var artifactoryBuild = req.body["select-build"];
    var ArtifactoryBuild = getArtifactoryBuild();
    ArtifactoryBuild.findOne({where: {repoUuid: repoUuid}}).then(data => {
        if (data == null) {
            var newArtifactoryBuild = new ArtifactoryBuild({
                repoUuid: repoUuid,
                artifactoryBuild: artifactoryBuild
            });
            newArtifactoryBuild.save(function (err) {
                if (err) {
                    res.send('Error occurred while creating a new associated Artifactory Build:' + err.message);
                } else {
                    console.log('ARTIFACTORY: CREATING NEW ASSOCIATION OF ' + repoUuid + ' WITH ' + artifactoryBuild);
                }
            });
        } else {
            data.artifactoryBuild = artifactoryBuild;
            data.save(function (err) {
                if (err) {
                    res.send('Error occurred while updating associated Artifactory Build:' + err.message);
                } else {
                    console.log('ARTIFACTORY: UPDATING EXISTING ASSOCIATION OF ' + repoUuid + ' WITH ' + artifactoryBuild);
                }
            });
        }
        var selected = [{'uri': artifactoryBuild}];
        res.render('associate-artifactory-build', {repoUuid: repoUuid, selected: selected});
    }).catch(function (error) {
        res.send('Error occurred while querying for an already existing associated Artifactory Build: ' + error);
    });
};

var artifactoryRequestOptions = function (path, repoUuid, callback) {
    var ArtifactoryUser = getArtifactoryUser();
    ArtifactoryUser.findOne({where: {repoUuid: repoUuid}}).then(data => {
        if (data != null && data.url != null && data.url !== "" && data.artifactoryUsername != null && data.artifactoryUsername !== "" && data.password != null && data.password !== "") {
            var options = {
                url: data.url + '/api/' + path,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                },
                auth: {
                    'username': data.artifactoryUsername,
                    "password": crypto.decrypt(data.password)
                }
            };
        } else {
            console.log("Artifactory User not found");
        }
        callback(options);
    }).catch(function (error) {
        console.log('Error occurred while querying for an already existing associated Artifactory Build: ' + error);
    });
};

module.exports = {
    getArtBuild,
    postArtBuild,
    artifactoryRequestOptions
};