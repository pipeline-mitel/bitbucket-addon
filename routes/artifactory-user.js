var crypto = require('./crypto-credentials');
var postArtUser = function (req, res) {
    var repoUuid = req.body.repoUuid;
    var url = req.body.url;
    var artifactoryUsername = req.body.username;
    var password = req.body.password;

    var ArtifactoryUser = getArtifactoryUser();
    ArtifactoryUser.findOne({where: {repoUuid: repoUuid}}).then(data => {
        if (data == null) {
            var newUser = new ArtifactoryUser({
                repoUuid: repoUuid,
                url: url,
                artifactoryUsername: artifactoryUsername,
                password: crypto.encrypt(password)
            });
            newUser.save(function (err) {
                if (err) {
                    res.send('Error occurred while creating a new associated Artifactory user:' + err.message);
                } else {
                    console.log('Artifactory: CREATING NEW ASSOCIATION OF ' + repoUuid + ' WITH ' + artifactoryUsername);
                }
            });
        } else {
            data.url = url;
            data.artifactoryUsername = artifactoryUsername;
            data.password = crypto.encrypt(password);
            data.save(function (err) {
                if (err) {
                    res.send('Error occurred while updating associated Artifactory user:' + err.message);
                } else {
                    console.log('Artifactory: UPDATING EXISTING ASSOCIATION OF ' + repoUuid + ' WITH ' + artifactoryUsername);
                }
            });
        }
        res.render('artifactory-user', {
            username: artifactoryUsername,
            password: password,
            url: url,
            repoUuid: repoUuid
        });
    }).catch(function (e) {
        res.send('Error occurred while querying for an already existing associated Artifactory user: ' + e);
    });
};


var getArtUser = function (req, res) {
    var repoUuid = req.query.repoUuid;
    var ArtifactoryUser = getArtifactoryUser();
    ArtifactoryUser.findOne({where: {repoUuid: repoUuid}}).then(data => {
        if (data == null) {
            res.render('artifactory-user', {
                repoUuid: repoUuid,
                url: null,
                username: null,
                password: null
            });
        } else {
            res.render('artifactory-user', {
                repoUuid: repoUuid,
                url: data.url,
                username: data.artifactoryUsername,
                password: crypto.decrypt(data.password)
            })
        }
    }).catch(function (error) {
        res.send('Error occurred while querying for an already existing associated Artifactory user: ' + error);
    })
};

module.exports = {
    getArtUser,
    postArtUser
};