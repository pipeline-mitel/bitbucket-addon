var request = require('request');
var bmbBuild = require('./associate-bamboo-build');

var trigger = function (req, res) {
    var repoUuid = req.query.repoUuid;
    var planKey = req.params.planKey;
    var post_data = '?stage&ExecuteAllStages';
    bmbBuild.bambooRequestOptions('queue/' + planKey, repoUuid, function (options) {
        options.headers = {'Content-Length': Buffer.byteLength(post_data)};
        options.method = 'POST';

        function callback(error, response, body) {
            if (!error && response.statusCode === 200) {
                console.log("Triggered Bamboo build: " + body);
            } else {
                console.log(error);
            }
        }

        request(options, callback);

    });
    res.redirect(req.get('referer'));
};

module.exports = {
    trigger
};