const crypto = require('crypto');
var algorithm = 'aes256';

var encrypt = function (credential) {
    var cipher = crypto.createCipher(algorithm, encryptionKey);

    let encrypted = cipher.update(credential, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    return encrypted;
};


var decrypt = function (credential) {
    var decipher = crypto.createDecipher(algorithm, encryptionKey);
    let decrypted = decipher.update(credential, 'hex', 'utf8') + decipher.final('utf8');
    return decrypted;
};

module.exports = {
    encrypt,
    decrypt
};