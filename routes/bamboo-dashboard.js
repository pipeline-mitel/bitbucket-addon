var request = require('request');
var parseString = require('xml2js').parseString;
var bmbBuild = require('./associate-bamboo-build');
var artBuild = require('./associate-artifactory-build');
var bintPackage = require('./associate-bintray-package');

var getDashboard = function (req, res) {
    const repoPath = req.query.repoPath;
    const repoUuid = req.query.repoUuid;
    const BambooBuild = getBambooBuild();
    BambooBuild.findOne({where: {repoUuid: repoUuid}}).then(data => {
        if (data == null) {
            var message = 'Bamboo build is not selected. Please Select Bamboo build';
            res.render('error', {message: message});
        } else {
            bmbBuild.bambooRequestOptions('result/' + data.bambooBuildKey, repoUuid, function (options) {
                if (options != null) {

                    function callback(error, response, body) {
                        if (!error && response.statusCode === 200) {
                            parseString(body, function (err, result) {
                                var builds = result.results.results[0].result;
                                builds.forEach(build => build.buildLink = build.link["0"].$.href.replace("rest/api/latest/result", "browse"));

                                var ArtifactoryBuild = getArtifactoryBuild();
                                ArtifactoryBuild.findOne({where: {repoUuid: repoUuid}}).then(data => {
                                    if (data == null) {
                                        console.log("Artifactory Build is not selected");
                                        res.render('jfrog', {'bambooBuildInfo': builds, 'repoUuid': repoUuid});
                                    } else {
                                        var artRequestPromises = builds.map(build => makeArtRequestPromise(build, data.artifactoryBuild, repoUuid));
                                        var bintRequestPromises = artRequestPromises.map(promise => makeBintRequestPromise(promise, repoUuid));
                                        Promise.all(bintRequestPromises).then(data => {
                                            res.render('jfrog', {'bambooBuildInfo': builds, 'repoUuid': repoUuid});
                                        }).catch(function (error) {
                                            const message = error;
                                            res.render('error', {message: message});
                                        });
                                    }
                                }).catch(function (e) {
                                    res.send('Error occurred while querying for an already existing associated Artifactory Build: ' + e);
                                });
                            });

                        } else {
                            console.log('problem with request: ' + error);
                        }
                    }

                    request(options, callback);
                } else {
                    var message = 'configured Bamboo user not found! ';
                    res.render('error', {message: message});
                }
            });


        }
    }).catch(function (e) {
        res.send('Error occurred while querying for an already existing associated Bamboo Build ' + e);
    });
};

var postDashboard = function (req, res) {
    console.log("This API is not implemented");
};


const makeArtRequestPromise = (build, artBuildName, repoUuid) => {
    return new Promise((resolve, reject) => {
        artBuild.artifactoryRequestOptions('build' + artBuildName + '/' + build.buildNumber[0], repoUuid, function (options, error) {
            if (options != null && !error) {
                function callback(error, response, body) {
                    if (!error && response.statusCode === 200) {
                        var list = JSON.parse(body);
                        (list.uri) ? build.buildUri = list.uri.replace("api/build", "webapp/#/builds") : list.uri;
                        build.buildInfo = list.buildInfo;
                        resolve(build);
                    } else {
                        resolve(build);
                    }
                }

                request(options, callback);
            } else if (error) {
                console.log('Error: ' + error);
                reject(error)
            } else {
                console.log('Artifactory User not found');
                reject('Artifactory User not found')
            }
        });
    });
};

const makeBintRequestPromise = (promise, repoUuid) => {

    return new Promise((resolve, reject) => {
        promise.then(build => {
            var BintrayPackage = getBintrayPackage();
            BintrayPackage.findOne({where: {repoUuid: repoUuid}}).then(data => {
                if ((data == null) || (data.bintrayPackage == null)) {
                    console.log("Package not found!");
                    reject("Package not found");
                } else {
                    let buildNumber = (build.buildInfo) ? build.buildInfo.number : " ";
                    let buildName = (build.buildInfo) ? build.buildInfo.name : " ";
                    const post_data = '[{"build.number":["' + buildNumber + '"]},{"build.name":["' + buildName + '"]}]';
                    const path = 'search/attributes/' + data.bintrayPackage + '/versions';
                    bintPackage.bintrayRequestOptionsPost(path, repoUuid, post_data, function (options) {
                        if (options != null) {
                            function callback(error, response, body) {
                                if (!error && response.statusCode === 200) {
                                    build.bintrayInfo = JSON.parse(body);
                                    resolve(build);
                                } else {
                                    resolve(build);
                                }
                            }

                            request(options, callback);
                        }
                    });
                }
            }).catch(function (error) {
                console.log('Error occurred while querying for an already existing associated Bintray package: ' + error);
            });

        }).catch(function (error) {
            console.log("Error" + error);
            reject(error);
        });
    });
};


module.exports = {
    getDashboard,
    postDashboard
};