var request = require('request');
var parseString = require('xml2js').parseString;
var crypto = require('./crypto-credentials');

var getBambBuild = function (req, res) {
    var repoUuid = req.query.repoUuid;
    var builds = [];
    console.log('FOUND UUID' + repoUuid);
    bambooRequestOptions('result', repoUuid, function (options) {
        if (options != null) {
            function callback(error, response, body) {
                if (!error && response.statusCode === 200) {
                    parseString(body, function (err, result) {
                        let build_object_list = result.results.results["0"];
                        let build_list = JSON.parse(JSON.stringify(build_object_list.result));
                        for (var key in build_list) {
                            var build = build_list[key];
                            builds.push({"buildKey": build.plan[0].$.key, "buildName": build.plan[0].$.name});
                        }
                        var BambooBuild = getBambooBuild();
                        BambooBuild.findOne({where: {repoUuid: repoUuid}}).then(data => {
                            if (data == null) {
                                res.render('associate-bamboo-build', {repoUuid: repoUuid, builds: builds});

                            } else {
                                var selected = [{
                                    "buildName": data.bambooBuildName,
                                    "buildKey": data.bambooBuildKey
                                }];
                                res.render('associate-bamboo-build', {
                                    repoUuid: repoUuid,
                                    builds: builds,
                                    selected: selected
                                });
                            }
                        }).catch(function (error) {
                            res.send('Error occurred while querying for an already existing associated Bamboo Build: ' + error);
                        });
                    });
                } else {
                    console.log('Got error fetching builds from Bamboo: ' + error);
                }
            }

            request(options, callback);

        } else {
            var message = 'configured Bamboo user not found! ';
            res.render('error', {message: message});
        }
    });
};

var postBambBuild = function (req, res) {
    var repoUuid = req.body.repoUuid;
    var bambooBuildName = req.body["select-build"];
    var bambooBuildKey = bambooBuildName.split(":")[1];
    var BambooBuild = getBambooBuild();
    BambooBuild.findOne({where: {repoUuid: repoUuid}}).then(data => {
        if (data == null) {
            var newBambooBuild = new BambooBuild({
                repoUuid: repoUuid,
                bambooBuildName: bambooBuildName,
                bambooBuildKey: bambooBuildKey
            });
            newBambooBuild.save(function (err) {
                if (err) {
                    res.send('Error occurred while creating a new associated Bamboo Build:' + err.message);
                } else {
                    console.log('BAMBOO: CREATING NEW ASSOCIATION OF ' + repoUuid + ' WITH ' + bambooBuildName);
                }
            });
        } else {
            data.bambooBuildName = bambooBuildName;
            data.bambooBuildKey = bambooBuildKey;
            data.save(function (err) {
                if (err) {
                    res.send('Error occurred while updating associated Bamboo Build:' + err.message);
                } else {
                    console.log('BAMBOO: UPDATING EXISTING ASSOCIATION OF ' + repoUuid + ' WITH ' + bambooBuildName);
                }
            });
        }
        var selected = [{"buildName": bambooBuildName, "buildKey": null}];
        res.render('associate-bamboo-build', {repoUuid: repoUuid, selected: selected});
    }).catch(function (error) {
        res.send('Error occurred while querying for an already existing associated Bamboo Build: ' + error);
    })
};

var bambooRequestOptions = function (path, repoUuid, callback) {
    var BambooUser = getBambooUser();
    BambooUser.findOne({where: {repoUuid: repoUuid}}).then(data => {
        if (data != null && data.url != null && data.url !== "" && data.bambooUsername != null && data.bambooUsername !== "" && data.password != null && data.password !== "") {
            var options = {
                url: data.url + 'rest/api/latest/' + path,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                },
                auth: {
                    'username': data.bambooUsername,
                    "password": crypto.decrypt(data.password)
                }
            };
        } else {
            console.log("Bamboo user not found!");
        }
        callback(options);
    }).catch(function (error) {
        console.log('Error occurred while querying for an already existing associated Bamboo user: ' + error);
    });
};

module.exports = {
    getBambBuild,
    postBambBuild,
    bambooRequestOptions
};