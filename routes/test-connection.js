var request = require('request');

var testArtifactoryConnection = function (reqQ, resQ) {
    var username = reqQ.query.username;
    var password = reqQ.query.password;
    var ArtifactoryUrl = reqQ.query.url;
    var options = {
        url: ArtifactoryUrl + '/api/system/ping',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
        auth: {
            'username': username,
            "password": password
        }
    };

    if (options != null) {
        function callback(error, response, body) {
            if (!error && response.statusCode === 200) {
                var status = body;
                resQ.send(status);
            } else {
                console.log("Got Error while checking Artifactory Connection:" + error);
                resQ.send(error);
            }
        }

        request(options, callback);
    }
};

var testBambooConnection = function (reqQ, resQ) {
    var username = reqQ.query.username;
    var password = reqQ.query.password;
    var bambooUrl = reqQ.query.url;
    var options = {
        url: bambooUrl + 'rest/api/latest/',
        headers: {
            'Content-Type': 'application/json'
        },
        method: 'GET',
        auth: {
            'username': username,
            "password": password
        }
    };
    if (options != null) {
        function callback(error, response, body) {
            if (!error && response.statusCode === 200) {
                var status = response.statusCode;
                resQ.send(status);
            } else {
                console.log("Got Error while checking Bamboo Connection:" + error);
                resQ.send(error);
            }
        }

        request(options, callback);
    }
};

module.exports = {
    testArtifactoryConnection,
    testBambooConnection
};