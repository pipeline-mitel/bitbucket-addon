var request = require('request');
var artBuild = require('./associate-artifactory-build');

var download = function (originalRequest, originalResponse) {
    var repoUuid = originalRequest.query.repoUuid;
    var buildName = originalRequest.params.buildName;
    var buildNumber = originalRequest.params.buildNumber;

    if (buildName === '' || buildName === ' ') {
        originalResponse.send('Build archive not available');
        originalResponse.end();
        return;
    }

    var file_name = buildName + "_" + buildNumber + ".tar.gz";
    var post_data = '{"buildName":"' + buildName + '","buildNumber":"' + buildNumber + '","archiveType":"tar.gz"}';
    var body = '';
    artBuild.artifactoryRequestOptions('archive/buildArtifacts', repoUuid, function (options) {
        options.headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/x-gzip',
            'Content-Length': Buffer.byteLength(post_data)
        };
        options.method = 'POST';
        options.body = post_data;
        originalResponse.setHeader('Content-disposition', 'attachment; filename=' + file_name);

        request(options).on('error', function (error) {
            console.log('problem with request: ' + error.message);
            originalResponse.send("Failed to download build archive: " + error.message);
        }).pipe(originalResponse);

    });
};

module.exports = {
    download
};