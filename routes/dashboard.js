var request = require('request');
var parseString = require('xml2js').parseString;
var bmbBuild = require('./associate-bamboo-build');
var artBuild = require('./associate-artifactory-build');
var bintPackage = require('./associate-bintray-package');

var getDashboard = function (req, res) {
    var bitBucketUsername = req.bitBucketUsername;
    var repoUuid = req.query.repoUuid;
    var BambooBuild = getBambooBuild();
    var buildInfo = [];
    BambooBuild.findOne({where: {repoUuid: repoUuid}}).then(data => {
        if (data == null) {
            var message = 'Bamboo build is not selected. Please Select Bamboo build';
            res.render('error', {message: message});
        } else {
            bmbBuild.bambooRequestOptions('result/' + data.bambooBuildKey, repoUuid, function (options) {
                if (options != null) {
                    var protocol = options.nameProtocol === "http" ? http : https;
                    var reqBambooGet = protocol.request(options, function (resBambooGet) {
                        console.log('STATUS: ' + resBambooGet.statusCode);
                        resBambooGet.setEncoding('utf8');
                        var rawData = [];
                        resBambooGet.on('data', function (chunk) {
                            rawData.push(chunk);
                        });
                        resBambooGet.on('end', function (resBambooGet) {
                            var chunk = rawData.join('');
                            reqBambooGet.write(chunk);
                            parseString(chunk, function (err, result) {
                                buildInfo = result.results.results[0].result;
                                var ArtifactoryBuild = getArtifactoryBuild();
                                ArtifactoryBuild.findOne({where: {repoUuid: repoUuid}}).then(data => {
                                    if (data == null) {
                                        console.log("Artifactory Build is not selected");
                                        res.render('jfrog', {
                                            bambooBuildInfo: buildInfo
                                        });
                                    } else {
                                        var list_object = [];
                                        for (var key in buildInfo) {
                                            (function (key) {
                                                list_object.push(key);
                                                var number = buildInfo[key].buildNumber[0];
                                                buildInfo[key].bitBucketUsername = bitBucketUsername;
                                                buildInfo[key].buildLink = buildInfo[key].link["0"].$.href.replace("rest/api/latest/result", "browse");
                                                artBuild.artifactoryRequestOptions('build' + data.artifactoryBuild + "/" + number, repoUuid, function (artifactoryOptions) {
                                                    if (artifactoryOptions != null) {
                                                        var artiProtocol = artifactoryOptions.nameProtocol === "http" ? http : https;
                                                        var reqGet = artiProtocol.request(artifactoryOptions, function (resGet) {
                                                            console.log('STATUS: ' + resGet.statusCode);
                                                            resGet.setEncoding('utf8');
                                                            var rawDataArti = [];
                                                            resGet.on('data', function (chunk) {
                                                                rawDataArti.push(chunk);
                                                            });
                                                            resGet.on('end', function (resGet) {
                                                                var chunkArti = rawDataArti.join('');
                                                                reqGet.write(chunkArti);
                                                                if (chunkArti.indexOf("<") > -1) {
                                                                    var artBuildInfo = " ";
                                                                } else {
                                                                    var artBuildInfo = JSON.parse(chunkArti);
                                                                }
                                                                if (artBuildInfo.buildInfo !== undefined && artBuildInfo.buildInfo.name !== undefined) {
                                                                    buildInfo[key].artName = artBuildInfo.buildInfo.name;
                                                                } else {
                                                                    buildInfo[key].artName = " ";
                                                                }
                                                                if (artBuildInfo.buildInfo !== undefined && artBuildInfo.buildInfo.principal !== undefined) {
                                                                    buildInfo[key].principal = artBuildInfo.buildInfo.principal;
                                                                } else {
                                                                    buildInfo[key].principal = " ";
                                                                }
                                                                if (artBuildInfo.buildInfo !== undefined && artBuildInfo.buildInfo.buildAgent !== undefined) {
                                                                    buildInfo[key].buildAgent = artBuildInfo.buildInfo.buildAgent;
                                                                } else {
                                                                    buildInfo[key].buildAgent = " ";
                                                                }
                                                                if (artBuildInfo.buildInfo !== undefined && artBuildInfo.buildInfo.number !== undefined) {
                                                                    buildInfo[key].artBuildNumber = artBuildInfo.buildInfo.number;
                                                                } else {
                                                                    buildInfo[key].artBuildNumber = " ";
                                                                }
                                                                if (artBuildInfo.uri !== undefined && artBuildInfo.uri !== undefined) {
                                                                    buildInfo[key].uri = artBuildInfo.uri.replace("api/build", "webapp/#/builds");
                                                                } else {
                                                                    buildInfo[key].uri = " ";
                                                                }
                                                                if (artBuildInfo.buildInfo !== undefined && artBuildInfo.buildInfo.started !== undefined) {
                                                                    buildInfo[key].promotionTime = artBuildInfo.buildInfo.started;
                                                                } else {
                                                                    buildInfo[key].promotionTime = " ";
                                                                }
                                                                if (artBuildInfo.buildInfo !== undefined && artBuildInfo.buildInfo.statuses !== undefined) {
                                                                    buildInfo[key].statuses = artBuildInfo.buildInfo.statuses;
                                                                } else {
                                                                    buildInfo[key].statuses = [];
                                                                }
                                                                var BintrayPackage = getBintrayPackage();
                                                                BintrayPackage.findOne({where: {repoUuid: repoUuid}}).then(data => {
                                                                    if ((data == null) || (data.bintrayPackage == null)) {
                                                                        console.log("Package not found!");
                                                                        list_object.splice(list_object.indexOf(key), 1);
                                                                        if (list_object.length === 0) {
                                                                            res.render('jfrog', {
                                                                                bambooBuildInfo: buildInfo
                                                                            });
                                                                        }
                                                                    } else {
                                                                        var post_data = '[{"build.number":["' + buildInfo[key].artBuildNumber + '"]},{"build.name":["' + buildInfo[key].artName + '"]}]';
                                                                        var path = 'search/attributes/' + data.bintrayPackage + '/versions';
                                                                        bintPackage.bintrayRequestOptionsPost(path, repoUuid, post_data, function (options) {
                                                                            if (options != null) {
                                                                                var post_req = https.request(options, function (resPost) {
                                                                                    resPost.setEncoding('utf8');
                                                                                    var output = [];
                                                                                    resPost.on('data', function (chunk) {
                                                                                        output.push(chunk);
                                                                                    });
                                                                                    resPost.on('end', function (resPost) {
                                                                                        var chunk = output.join('');
                                                                                        if (chunk !== "[]" && chunk !== "") {
                                                                                            var vesion = JSON.parse(chunk);
                                                                                            buildInfo[key].version = vesion[0].name;
                                                                                            buildInfo[key].repo = vesion[0].repo;
                                                                                            buildInfo[key].package = vesion[0].package;
                                                                                            buildInfo[key].owner = vesion[0].owner;
                                                                                        } else {
                                                                                            buildInfo[key].version = " ";
                                                                                            buildInfo[key].repo = " ";
                                                                                            buildInfo[key].package = " ";
                                                                                            buildInfo[key].owner = " ";
                                                                                        }
                                                                                        list_object.splice(list_object.indexOf(key), 1);
                                                                                        if (list_object.length === 0) {
                                                                                            res.render('jfrog', {
                                                                                                bambooBuildInfo: buildInfo
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                });
                                                                                post_req.write(post_data);
                                                                                post_req.on('error', function (err) {
                                                                                    console.log('error: ' + err.message);
                                                                                });
                                                                                post_req.end();
                                                                            }
                                                                            else {
                                                                                list_object.splice(list_object.indexOf(key), 1);
                                                                                if (list_object.length === 0) {
                                                                                    res.render('jfrog', {
                                                                                        bambooBuildInfo: buildInfo
                                                                                    });
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                }).catch(function (e) {
                                                                    res.send('Error occurred while querying for an already existing associated Bintray package: ' + e);
                                                                });
                                                            });
                                                        });
                                                        reqGet.on('error', function (e) {
                                                            console.log('problem with request: ' + e.message);
                                                        });
                                                        reqGet.end();
                                                    } else {
                                                        var message = 'configured Artifactory user not found!';
                                                        list_object.splice(list_object.indexOf(key), 1);
                                                        if (list_object.length === 0) {
                                                            res.render('jfrog', {
                                                                bambooBuildInfo: buildInfo
                                                            });
                                                        }
                                                    }
                                                });
                                            })(key);
                                        }
                                    }
                                }).catch(function (e) {
                                    res.send('Error occurred while querying for an already existing associated Artifactory Build: ' + e);
                                });
                            });
                        });
                    });
                    reqBambooGet.on('error', function (e) {
                        console.log('problem with request: ' + e.message);
                    });
                    reqBambooGet.end();
                } else {
                    var message = 'configured Bamboo user not found! ';
                    res.render('error', {message: message});
                }
            });


        }
    }).catch(function (e) {
        res.send('Error occurred while querying for an already existing associated Bamboo Build ' + e);
    });
};

var postDashboard = function (req, res) {
    console.log("This API is not implemented");
};


module.exports = {
    getDashboard,
    postDashboard
};