var crypto = require('./crypto-credentials');

var postBintUser = function (req, res) {
    var repoUuid = req.body.repoUuid;
    var bintrayUsername = req.body.username;
    var apiKey = req.body.apiKey;

    var BintrayUser = getBintrayUser();
    BintrayUser.findOne({where: {repoUuid: repoUuid}}).then(data => {
        if (data == null) {
            var newUser = new BintrayUser({
                repoUuid: repoUuid,
                bintrayUsername: bintrayUsername,
                apiKey: crypto.encrypt(apiKey)
            });
            newUser.save(function (err) {
                if (err) {
                    res.send('Error occurred while creating a new associated Bintray user:' + err.message);
                } else {
                    console.log('BINTRAY: CREATING NEW ASSOCIATION OF ' + repoUuid + ' WITH ' + bintrayUsername);
                }
            });
        } else {
            data.bintrayUsername = bintrayUsername;
            data.apiKey = crypto.encrypt(apiKey);
            data.save(function (err) {
                if (err) {
                    res.send('Error occurred while updating associated Bintray user:' + err.message);
                } else {
                    console.log('BINTRAY: UPDATING EXISTING ASSOCIATION OF ' + repoUuid + ' WITH ' + bintrayUsername);
                }
            });
        }
        res.render('bintray-user', {username: bintrayUsername, apiKey: apiKey, repoUuid: repoUuid});
    }).catch(function (e) {
        res.send('Error occurred while querying for an already existing associated Bintray user: ' + e);
    });
};

var getBintUser = function (req, res) {
    var repoUuid = req.query.repoUuid;
    var BintrayUser = getBintrayUser();
    BintrayUser.findOne({where: {repoUuid: repoUuid}}).then(data => {
        if (data == null) {
            res.render('bintray-user', {username: null, apiKey: null, repoUuid: repoUuid});
        } else {
            res.render('bintray-user', {username: data.bintrayUsername, apiKey: crypto.decrypt(data.apiKey), repoUuid: repoUuid})
        }
    }).catch(function (error) {
        res.send('Error occurred while querying for an already existing associated Bintray user: ' + error);
    });
};

module.exports = {
    getBintUser,
    postBintUser
};