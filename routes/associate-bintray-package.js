var crypto = require('./crypto-credentials');

var getBintPackage = function (req, res) {
    var repoUuid = req.query.repoUuid;
    console.log('FOUND UUID' + repoUuid);
    var BintrayPackage = getBintrayPackage();
    BintrayPackage.findOne({where: {repoUuid: repoUuid}}).then(data => {
        if (data == null) {
            res.render('associate-bintray-package', {repoUuid: repoUuid, packagePath: null});
        } else {
            res.render('associate-bintray-package', {repoUuid: repoUuid, packagePath: data.bintrayPackage})
        }
    }).catch(function (error) {
        res.send('Error occurred while querying for an already existing associated Bintray package: ' + error);
    });
};

var postBintPackage = function (req, res) {
    var repoUuid = req.body.repoUuid;
    var packagePath = req.body.packagePath;

    var BintrayPackage = getBintrayPackage();
    BintrayPackage.findOne({where: {repoUuid: repoUuid}}).then(data => {
        if (data == null) {
            var newPackage = new BintrayPackage({
                repoUuid: repoUuid,
                bintrayPackage: packagePath
            });
            newPackage.save(function (err) {
                if (err) {
                    res.send('Error occurred while creating a new associated Bintray package:' + err.message);
                } else {
                    console.log('BINTRAY: CREATING NEW ASSOCIATION OF ' + repoUuid + ' WITH ' + packagePath);
                }
            });
        } else {
            data.bintrayPackage = packagePath;
            data.save(function (err) {
                if (err) {
                    res.send('Error occurred while updating associated Bintray package:' + err.message);
                } else {
                    console.log('BINTRAY: UPDATING EXISTING ASSOCIATION OF ' + repoUuid + ' WITH ' + packagePath);
                }
            });
        }
        res.render('associate-bintray-package', {repoUuid: repoUuid, packagePath: packagePath});
    }).catch(function (error) {
        res.send('Error occurred while querying for an already existing associated Bintray package: ' + error);
    });
};

var bintrayRequestOptionsPost = function (path, repoUuid, post_data, callback) {
    var BintrayUser = getBintrayUser();
    BintrayUser.findOne({where: {repoUuid: repoUuid}}).then(data => {
        if (data != null && data.bintrayUsername != null && data.bintrayUsername !== "" && data.apiKey != null && data.apiKey !== "") {
            var options = {
                url: 'https://api.bintray.com/' + path,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(post_data)
                },
                body: post_data,
                auth: {
                    'username': data.bintrayUsername,
                    "password": crypto.decrypt(data.apiKey)
                }
            };
        } else {
            console.log("Bintray User not found");
        }
        callback(options);
    }).catch(function (error) {
        console.log('Error occurred while querying for an already existing associated Artifactory Build: ' + error);
    });
};

module.exports = {
    getBintPackage,
    postBintPackage,
    bintrayRequestOptionsPost
};